package com.poc.jioassessmenttest.model;

import android.text.TextUtils;

import java.util.Date;
import java.util.List;

public class JACalendarEventModel {

    private Date mDate;

    private List<String> eventList;

    private String eventString;

    public JACalendarEventModel(Date mDate) {
        this.mDate = mDate;
    }

    public Date getmDate() {
        return mDate;
    }

    public void setmDate(Date mDate) {
        this.mDate = mDate;
    }

    public Boolean getEventAvailable() {
        return getEventList() != null && !getEventList().isEmpty();
    }

    public List<String> getEventList() {
        return eventList;
    }

    public void setEventList(List<String> eventList) {
        this.eventList = eventList;
        setEvents();
    }

    public void setEvents(){
        String tempString = "";
        if(getEventAvailable()) {
            for (String item : getEventList()) {
                if (TextUtils.isEmpty(tempString)) {
                    tempString = item;
                } else {
                    tempString = tempString + "\n" + item;
                }
            }
        }
        eventString = tempString;
    }

    public String getEvents() {
        return eventString;
    }
}
