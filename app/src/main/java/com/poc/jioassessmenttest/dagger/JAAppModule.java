package com.poc.jioassessmenttest.dagger;


import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class JAAppModule {
    private Context mJAApplication;

    public JAAppModule(Context context) {
        this.mJAApplication = context;
    }

    @Provides
    @Singleton
    Context provideApplication() {
        return mJAApplication;
    }
}