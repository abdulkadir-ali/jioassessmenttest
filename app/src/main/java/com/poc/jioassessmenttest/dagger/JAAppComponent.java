package com.poc.jioassessmenttest.dagger;


import com.poc.jioassessmenttest.network.JANetworkModule;
import com.poc.jioassessmenttest.view.activities.JABaseActivity;
import com.poc.jioassessmenttest.view.fragments.JABaseFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * This specifies the contract between the view and the Presenter.
 */
@Singleton
@Component(modules = {JAAppModule.class, JANetworkModule.class})
public interface JAAppComponent {

    void inject(JABaseActivity baseActivity);

    void inject(JABaseFragment baseFragment);
}
