package com.poc.jioassessmenttest.interfaces;

/**
 * Base view to update view from presenter.
 */
public interface JABaseView {

    void showProgress();

    void hideProgress();
}
