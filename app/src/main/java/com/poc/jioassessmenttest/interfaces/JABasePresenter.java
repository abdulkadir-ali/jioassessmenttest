package com.poc.jioassessmenttest.interfaces;

/**
 * Base presenter to know activity callback at presenter.
 */
public interface JABasePresenter {

    void start();

    void resume();

    void pause();

    void stop();

    void destroy();
}
