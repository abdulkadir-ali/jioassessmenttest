

package com.poc.jioassessmenttest.network;

import com.poc.jioassessmenttest.model.JACalendarEventModel;

import java.util.ArrayList;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Helper class to handle network calls
 */

public class JANetworkManager {

    private JARetrofitInterface mRetrofitInterface;

    public JANetworkManager(JARetrofitInterface RetrofitInterface) {
        mRetrofitInterface = RetrofitInterface;
    }

    public Subscription getCalendarEventList(JAResponseListener<ArrayList<JACalendarEventModel>> callback) {
        return mRetrofitInterface.getCalendarEventData(JANetworkUtils.getURL(JANetworkUtils.REQ_LOCATION))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends ArrayList<JACalendarEventModel>>>() {
                    @Override
                    public Observable<? extends ArrayList<JACalendarEventModel>> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(callback);
    }
}
