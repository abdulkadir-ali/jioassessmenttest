package com.poc.jioassessmenttest.network;

import com.poc.jioassessmenttest.model.JACalendarEventModel;

import java.util.ArrayList;

import retrofit2.http.GET;
import retrofit2.http.Url;
import rx.Observable;

public interface JARetrofitInterface {

    @GET()
    Observable<ArrayList<JACalendarEventModel>> getCalendarEventData(@Url String url);
}
