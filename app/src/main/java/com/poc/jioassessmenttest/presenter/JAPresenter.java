package com.poc.jioassessmenttest.presenter;

import com.poc.jioassessmenttest.contract.JAPresenterContract;
import com.poc.jioassessmenttest.model.JACalendarEventModel;
import com.poc.jioassessmenttest.network.JANetworkManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import rx.subscriptions.CompositeSubscription;

public class JAPresenter implements JAPresenterContract.Presenter {

    private JANetworkManager networkWrapper;
    private JAPresenterContract.View view;
    private CompositeSubscription subscriptions;

    private List<JACalendarEventModel> dates;

    public JAPresenter() {
        dates = new ArrayList<JACalendarEventModel>();
    }

    public JAPresenter(JANetworkManager networkWrapper, JAPresenterContract.View view) {
        this.networkWrapper = networkWrapper;
        this.view = view;
        subscriptions = new CompositeSubscription();
        dates = new ArrayList<JACalendarEventModel>();
    }

    @Override
    public void getCalendarEvents() {

    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void getDateRange() {

        if(!dates.isEmpty())
            view.setDateRangeData(dates);

        view.setDateRangeData(getDateRangeList());
    }

    public List<JACalendarEventModel> getDateRangeList() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

        String startDate = "01/01/1900";

        Date date = null;

        try {
            date = simpleDateFormat.parse(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date);


        Calendar cal2 = Calendar.getInstance();

        while(!cal1.after(cal2))
        {
            dates.add(new JACalendarEventModel(cal2.getTime()));
            cal2.add(Calendar.DATE, -1);
        }
        return dates;
    }

    public String getStartDate(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return simpleDateFormat.format(dates.get(dates.size()-1).getmDate());
    }

    @Override
    public void getRandomDateRange() {

        view.setDateRangeData(getRandomDateRangeList());
    }

    public List<JACalendarEventModel> getRandomDateRangeList(){
        int min = 1900;
        int max = 2018;

        Random r = new Random();
        int startYear = r.nextInt(max - min + 1) + min;
        int endYear = startYear + 10;

        List<JACalendarEventModel> dates = new ArrayList<JACalendarEventModel>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

        String startDateString = "01/01/" + startYear;
        String endDateString = "31/12/" + endYear;

        Date startDate = null, endDate = null;
        try {
            startDate = simpleDateFormat.parse(startDateString);
            endDate = simpleDateFormat.parse(endDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(startDate);


        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(endDate);

        Calendar currentDate = Calendar.getInstance();
        while(!cal1.after(cal2))
        {
            if(!cal2.after(currentDate))
                dates.add(new JACalendarEventModel(cal2.getTime()));
            cal2.add(Calendar.DATE, -1);
        }

        setupEvents(dates);
        return dates;
    }


    private void setupEvents(List<JACalendarEventModel> dates) {
        Random r = new Random();
        for (int i=0; i<1001; i++) {
            int randomEntry = r.nextInt(dates.size());
            if(dates.size() > randomEntry) {
                JACalendarEventModel calendarEventModel = dates.get(randomEntry);
                List<String> eventList = calendarEventModel.getEventList();
                if(eventList == null || eventList.isEmpty()) {
                    eventList = new ArrayList<String>();
                    eventList.add("Test Event " + i);
                }else{
                    eventList.add("Test Event " + i);
                }
                calendarEventModel.setEventList(eventList);
            }

        }


    }
}
