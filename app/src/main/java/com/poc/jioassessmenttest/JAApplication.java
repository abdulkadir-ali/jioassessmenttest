package com.poc.jioassessmenttest;

import android.app.Application;
import android.content.Context;

import com.poc.jioassessmenttest.dagger.DaggerJAAppComponent;
import com.poc.jioassessmenttest.dagger.JAAppComponent;
import com.poc.jioassessmenttest.dagger.JAAppModule;
import com.poc.jioassessmenttest.network.JANetworkModule;

public class JAApplication extends Application {

    private static Context context;
    private JAAppComponent mJAAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mJAAppComponent = DaggerJAAppComponent.builder()
                .jAAppModule(new JAAppModule(this))
                .jANetworkModule(new JANetworkModule(getContext()))
                .build();
    }

    public JAAppComponent getJAAppComponent() {
        return mJAAppComponent;
    }

    public JAApplication() {
        context = this;
    }

    public static Context getContext() {
        return context;
    }
}
