package com.poc.jioassessmenttest.view.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.poc.jioassessmenttest.util.JAFragmentConstant;
import com.poc.jioassessmenttest.util.JAFragmentUtils;

/**
 * Adapter class to display fragment inside view pager.
 */

public class JAPagerAdapter extends FragmentStatePagerAdapter {

    private Context context;

    private SparseArray<Fragment> fragmentArray = new SparseArray<Fragment>();

    public JAPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        fragmentArray.put(position, fragment);
        return fragment;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return Fragment.instantiate(context,
                        JAFragmentUtils.getFragmentTag(JAFragmentConstant.FRAGMENT_CALENDAR_EVENT), null);
            case 1:
                return Fragment.instantiate(context,
                        JAFragmentUtils.getFragmentTag(JAFragmentConstant.FRAGMENT_DUMMY_1), null);
            case 2:
                return Fragment.instantiate(context,
                        JAFragmentUtils.getFragmentTag(JAFragmentConstant.FRAGMENT_DUMMY_2), null);
            default:
                return Fragment.instantiate(context,
                        JAFragmentUtils.getFragmentTag(JAFragmentConstant.FRAGMENT_CALENDAR_EVENT), null);
        }
    }

    @Override
    public int getCount() {
        return JAFragmentConstant.PAGER_FRAGMENT_COUNT;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        fragmentArray.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getFragment(int position) {
        return fragmentArray.get(position);
    }
}