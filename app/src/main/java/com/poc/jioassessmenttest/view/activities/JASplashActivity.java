package com.poc.jioassessmenttest.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import com.poc.jioassessmenttest.R;

public class JASplashActivity extends JABaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        launchLoginScreen();
    }

    private void launchLoginScreen(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(JASplashActivity.this, JAMainActivity.class));
                finish();
            }
        }, 4000);
    }
}
