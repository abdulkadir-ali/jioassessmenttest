package com.poc.jioassessmenttest.view.activities;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.poc.jioassessmenttest.R;
import com.poc.jioassessmenttest.view.adapter.JAPagerAdapter;
import com.poc.jioassessmenttest.view.fragments.JABaseFragment;
import com.viewpagerindicator.CirclePageIndicator;

import butterknife.BindView;
import butterknife.ButterKnife;

public class JAMainActivity extends JABaseActivity {

    @BindView(R.id.view_pager)
    ViewPager viewPager;

    @BindView(R.id.pager_indicator)
    CirclePageIndicator pagerIndicator;

    private JAPagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initUI();
    }

    private void initUI() {
        pagerAdapter = new JAPagerAdapter(this, getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        pagerIndicator.setViewPager(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                invalidateOptionsMenu();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        JABaseFragment baseFragment = (JABaseFragment) pagerAdapter.getFragment(viewPager.getCurrentItem());
        switch (item.getItemId()) {
            case R.id.action_range :
                baseFragment.updateRange();
                break;
            case R.id.action_clear:
                baseFragment.clearRange();
                break;
        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if(viewPager.getCurrentItem() != 0){
            menu.setGroupVisible(R.id.group_range, false);
        } else {
            menu.setGroupVisible(R.id.group_range, true);
        }
        return super.onPrepareOptionsMenu(menu);
    }
}
