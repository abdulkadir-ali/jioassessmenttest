package com.poc.jioassessmenttest.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.poc.jioassessmenttest.R;
import com.poc.jioassessmenttest.model.JACalendarEventModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Calendar Dates and Event Adapter.
 */
public class JARecyclerAdapter extends RecyclerView.Adapter<JARecyclerAdapter.MyViewHolder> {

    private LayoutInflater layoutInflater;
    private List<JACalendarEventModel> dateList = new ArrayList<JACalendarEventModel>();
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MMM/yyyy");

    public JARecyclerAdapter(Context context) {
        layoutInflater = LayoutInflater.from(context);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvItem, tvEvent;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvItem = (TextView) itemView.findViewById(R.id.tv_item);
            tvEvent = (TextView) itemView.findViewById(R.id.tv_events);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_recycler_view, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        JACalendarEventModel calendarEventModel = dateList.get(position);
        holder.tvItem.setText(simpleDateFormat.format(calendarEventModel.getmDate()));
        if(calendarEventModel.getEventAvailable() != null && calendarEventModel.getEventAvailable()){
            holder.tvEvent.setVisibility(View.VISIBLE);
            holder.tvEvent.setText(calendarEventModel.getEvents());
        }else {
            holder.tvEvent.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return dateList.size();
    }

    public void setListContent(List<JACalendarEventModel> items){
        this.dateList = items;
        notifyDataSetChanged();
    }
}