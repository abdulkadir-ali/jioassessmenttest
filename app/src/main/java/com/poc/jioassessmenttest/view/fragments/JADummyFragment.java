package com.poc.jioassessmenttest.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.poc.jioassessmenttest.R;

public class JADummyFragment extends JABaseFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dummy, container, false);
        return rootView;
    }

    @Override
    public void updateRange() {

    }

    @Override
    public void clearRange() {

    }
}
