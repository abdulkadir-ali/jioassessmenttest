package com.poc.jioassessmenttest.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.poc.jioassessmenttest.JAApplication;
import com.poc.jioassessmenttest.network.JANetworkManager;

import javax.inject.Inject;

public abstract class JABaseFragment extends Fragment {

    @Inject
    public JANetworkManager networkManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((JAApplication) getActivity().getApplicationContext()).getJAAppComponent().inject(this);
    }

    public abstract void updateRange();

    public abstract void clearRange();

    protected int FRAG_ADD = 1;
    protected int FRAG_REPLACE = 2;
    protected int FRAG_ADD_ANIMATE = 3;
    protected int FRAG_DIALOG = 4;
    protected int FRAG_REPLACE_WITH_STACK = 5;
    protected int FRAG_ADD_WITH_STACK = 6;
}
