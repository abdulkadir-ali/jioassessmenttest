package com.poc.jioassessmenttest.view.activities;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;

import com.poc.jioassessmenttest.JAApplication;
import com.poc.jioassessmenttest.network.JANetworkManager;

import javax.inject.Inject;

public class JABaseActivity extends AppCompatActivity {

    @Inject
    JANetworkManager networkManager;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        ((JAApplication) getApplicationContext()).getJAAppComponent().inject(this);
    }
}
