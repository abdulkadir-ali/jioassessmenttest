package com.poc.jioassessmenttest.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.poc.jioassessmenttest.R;
import com.poc.jioassessmenttest.contract.JAPresenterContract;
import com.poc.jioassessmenttest.model.JACalendarEventModel;
import com.poc.jioassessmenttest.presenter.JAPresenter;
import com.poc.jioassessmenttest.view.adapter.JARecyclerAdapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Fragment class to display dates and calendar events.
 */
public class JACalendarEventFragment extends JABaseFragment implements JAPresenterContract.View {

    private static final String TAG = JACalendarEventFragment.class.getSimpleName();

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private Unbinder unbinder;

    private JAPresenter presenter;

    private JARecyclerAdapter recyclerAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_calendarevents, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initData();
    }

    private void initData() {
        presenter = new JAPresenter(networkManager, this);
        recyclerAdapter = new JARecyclerAdapter(getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(recyclerAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);
        presenter.getDateRange();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void onSuccess(List<JACalendarEventModel> dataList) {

    }

    @Override
    public void showError(int stringResource) {

    }

    @Override
    public void updateRange() {
        Log.d(TAG, "Update Range Called");
        presenter.getRandomDateRange();
    }

    @Override
    public void clearRange() {
        Log.d(TAG, "Clear Range Called");
        presenter.getDateRange();
    }

    @Override
    public void setDateRangeData(List<JACalendarEventModel> dateRangeData) {
        recyclerAdapter.setListContent(dateRangeData);
    }
}
