package com.poc.jioassessmenttest.util;

/**
 * Class to handle all fragments constants
 */
public class JAFragmentConstant {

    public static final int PAGER_FRAGMENT_COUNT = 3;

    public static final int FRAGMENT_CALENDAR_EVENT = 1;
    public static final int FRAGMENT_DUMMY_1 = 2;
    public static final int FRAGMENT_DUMMY_2 = 3;
}
