package com.poc.jioassessmenttest.util;

import android.support.v4.app.Fragment;

import com.poc.jioassessmenttest.view.fragments.JACalendarEventFragment;
import com.poc.jioassessmenttest.view.fragments.JADummyFragment;

/**
 * Class to get Fragment Tag
 */

public class JAFragmentUtils {

    public static String getFragmentTag(int type) {
        switch (type) {
            case JAFragmentConstant.FRAGMENT_CALENDAR_EVENT:
                return JACalendarEventFragment.class.getName();

            case JAFragmentConstant.FRAGMENT_DUMMY_1:
                return JADummyFragment.class.getName();

            case JAFragmentConstant.FRAGMENT_DUMMY_2:
                return JADummyFragment.class.getName();

            default:
                return Fragment.class.getName();
        }
    }
}
