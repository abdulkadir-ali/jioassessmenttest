package com.poc.jioassessmenttest.contract;

import com.poc.jioassessmenttest.interfaces.JABasePresenter;
import com.poc.jioassessmenttest.interfaces.JABaseView;
import com.poc.jioassessmenttest.model.JACalendarEventModel;

import java.util.List;

/**
 * Contract between view and presenter.
 */
public interface JAPresenterContract {

    interface View extends JABaseView {
        void onSuccess(List<JACalendarEventModel> dataList);

        void showError(int stringResource);

        void setDateRangeData(List<JACalendarEventModel> dateRangeData);
    }

    interface Presenter extends JABasePresenter {
        void getCalendarEvents();

        void getDateRange();

        void getRandomDateRange();
    }
}
