package com.poc.jioassessmenttest;

import com.poc.jioassessmenttest.presenter.JAPresenter;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Test cases for testing the presenter functionality
 */

public class JACalendarFragmentTest {

    JAPresenter presenter;

    @Before
    public void setup() {
        presenter = new JAPresenter();
    }
    @Test
    public void generateRangeTest() {
        assertNotNull(presenter.getDateRangeList());
        assertNotNull(presenter.getRandomDateRangeList());
    }

    @Test
    public void checkStartDate() {
        presenter.getDateRangeList();
        assertEquals("01/01/1900", presenter.getStartDate());
    }



}
