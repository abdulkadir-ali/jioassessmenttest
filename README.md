# JIOAssessmentTest

Requirements :
- Implement view pager with three fragments.
- One fragment should display recycler view and other two fragments are dummy fragments.
- Recyler view should display range between 01/01/1900 to current date.
- Implement option menu to filter the date result to any random 10 years of duration.
- When filter it should display the Events below to date row.
- Should use google calendar API to fetch the calendar events.
- Provide few JUnit test cases.


Implementation Details :
- Follow MVP architecture.
- Used Dagger2, retrofit, gson components for network calls.
- Used butterknife, pager indicator component for views.
- Supports minSdk version 19 and target sdk version is 22.
- Implemented splash screen.
- Implemented Main Activity with View Pager.
- Implemented Calendar Event fragment.
- Implemented recycler view and generating range for dates.
- Implemented option menu to generate range of dates (10 years) and clearing the same.
- Showing random generated events below to the dates.
- Write couple of test to check start date and null for date range.
- Created keystore and product flavor to support debug/release build enviornment.
- Tested on Emulator running on Android 8.1.0.


Known Issues:
- Orientation is not handled.
- Google calendar integration is not done (need to handle oauth token didn't get much time to do this stuff, network wrapper and retrofit implementation is fully done)
- Need to write few more test cases.

